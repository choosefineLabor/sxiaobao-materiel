package com.choosefine.base.materiel.client.demo;

import lombok.Data;

/**
 * Created by Administrator on 2017/7/26.
 */

public class DemoDto {
    private Integer id;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    private String code;
}
