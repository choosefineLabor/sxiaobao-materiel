package com.choosefine.base.materiel.common.util;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

/**
 * @Author hujj
 * @Date 2016/12/12
 */
public class MapperFilterUtil {
    public void setFilter(ObjectMapper objectMapper, Class<?> target) throws IllegalAccessException, InstantiationException {
        String className = target.newInstance().getClass().getSimpleName();
        switch (className) {
            case "PageInfo":
                objectMapper.setFilterProvider(filter("objectFilter", "size","startRow","endRow","firstPage","prePage","nextPage","lastPage","isFirstPage","isLastPage","hasPreviousPage","hasNextPage","navigatePages","sort","direction","navigatepageNums"));
                objectMapper.addMixIn(target.newInstance().getClass(), ObjectFilterMixIn.class);
                break;
            default:
                break;
        }
    }

    @JsonFilter("objectFilter")
    interface ObjectFilterMixIn {
    }

    private FilterProvider filter(String filterName, String... propertyes) {
        // 过滤不想要的
        FilterProvider filter = new SimpleFilterProvider().addFilter(
                filterName,
                SimpleBeanPropertyFilter.serializeAllExcept(propertyes));
        return filter;
    }
}
