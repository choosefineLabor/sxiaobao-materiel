package com.choosefine.base.materiel.service.demo;


import com.choosefine.base.materiel.client.demo.DemoDto;

/**
 * Created by Administrator on 2017/7/26.
 */
public interface DemoService {

    /**
     * 通过id获取
     *
     * @param id
     * @return
     */
    DemoDto get(Integer id);
}
