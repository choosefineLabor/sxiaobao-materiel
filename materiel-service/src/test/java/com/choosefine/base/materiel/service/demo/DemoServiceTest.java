package com.choosefine.base.materiel.service.demo;

import com.choosefine.base.materiel.client.demo.DemoDto;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Map;

/**
 * Created by Jersey on 2017/7/26.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:test-service-context.xml")
@Rollback(value = false)
@Transactional(transactionManager = "xxManager")
public class DemoServiceTest {
    @Autowired
    private DemoService demoService;

    private DataSource dataSource;


    @Test
    public void get() {
        DemoDto demoDto = demoService.get(1);
        QueryRunner runner = new QueryRunner(dataSource);
        String sql = "select * from demo limit 1";
        Map<String, Object> result = null;
        try {
            result = runner.query(sql, new MapHandler());
        } catch (SQLException e) {
            //assertTrue(false);
            e.printStackTrace();

        }
        for (Map.Entry<String, Object> entry : result.entrySet()) {
            //
            //assertTrue(demoDto与entry相同);

        }

        //assertTrue(false);

    }

}
